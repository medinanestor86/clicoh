import requests
from urllib.parse import urljoin

from django.core.cache import cache
from services.externals.dolarsi.constans import DollarEnum


class Consumer:
    api_base = "https://www.dolarsi.com"
    dollar_values = [DollarEnum.DOLLAR_OFICIAL, DollarEnum.DOLLAR_BLUE]
    dollar_values_in_cache = "dollar_values"

    @classmethod
    def get_values(cls):
        if cache.get(cls.dollar_values_in_cache, None):
            return cache.get(cls.dollar_values_in_cache)
        else:
            api_url = urljoin(cls.api_base, "api/api.php?type=valoresprincipales")
            response_service = requests.get(api_url)
            response_service.raise_for_status()
            response_json = response_service.json()
            values = {
                item["casa"]["nombre"].upper().replace(" ", "_"): item["casa"]["venta"].replace(",", ".")
                for item in response_json
                if item["casa"]["nombre"].upper().replace(" ", "_") in cls.dollar_values
            }
            cache.set(cls.dollar_values_in_cache, values, 60 * 60)
            return values
