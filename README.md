CLICOH TESTS
============

###### LEVANTAR SERVICIO DE FORMA LOCAL

`docker-compose -f docker-compose-local.yml up`

###### EJECUTAR TESTS

`docker-compose -f docker-compose-local.yml run api python manage.py test -v 3`


###### API DOCUMETACION

`http://0.0.0.0:8000/docs/`
