import dj_database_url
import django_on_heroku

from .base import *

DATABASES = {
    "default": dj_database_url.config()
}

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
django_on_heroku.settings(locals())
