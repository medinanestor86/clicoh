FROM python:3.8-alpine
RUN apk update && apk add bash
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
RUN apk upgrade && apk add --no-cache --virtual build-dependencies linux-headers postgresql-client \
postgresql-dev linux-headers make gcc g++ libffi-dev libxml2-dev libxslt-dev libldap \
libsasl jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev \
tk-dev tcl-dev && rm -rf /var/cache/apk/*

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0



# create root directory for our project in the container
RUN mkdir /code

# Set the working directory to /philosopheat_app
WORKDIR /code

# add requirements in code
ADD config/deploy/requirements  /code/requirements

# Copy the current directory contents into the container at /philosopheat_app
ADD . /code/

# Install any needed packages specified in requirements.txt
RUN pip install -r requirements/develop.txt

# collect static files
RUN python manage.py collectstatic --noinput

# add and run as non-root user
RUN adduser -D api
USER api

# run gunicorn
CMD gunicorn config.wsgi:application --bind 0.0.0.0:$PORT
