from rest_framework import serializers

from apps.orders.models import Order, OrderDetail


class OrderSerializer(serializers.ModelSerializer):
    total = serializers.SerializerMethodField(source="get_total")
    total_usd = serializers.SerializerMethodField(source="get_total_usd")

    class Meta:
        model = Order
        fields = "__all__"

    def get_total(self, obj):
        return obj.get_total()

    def get_total_usd(self, obj):
        return obj.get_total_usd()


class OrderProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = "__all__"
        read_only_fields = ("order",)

    def validate(self, attrs):
        product = self.instance.product if self.instance else attrs["product"]
        quantity = attrs["quantity"]
        order = self.instance.order if self.instance else self.context["order"]
        product_in_detail = order.products.filter(product=product).first()
        stock_total = product.stock if product_in_detail is None else product.stock + product_in_detail.quantity
        if stock_total < quantity:
            raise serializers.ValidationError(f"product {product.name} does not have enough stock")
        return attrs

    def validate_quantity(self, value):
        if value < 1:
            raise serializers.ValidationError("the assigned amount must be greater than or equal to 1")
        return value

    def create(self, validated_data):
        return OrderDetail.objects.add_product(
            order_id=validated_data["order"].pk, product_id=validated_data["product"].pk,
            quantity=validated_data["quantity"]
        )

    def update(self, instance, validated_data):
        return OrderDetail.objects.add_product(
            order_id=instance.order_id, product_id=instance.product_id,
            quantity=validated_data["quantity"]
        )
    
    def destroy(self):
        super(OrderProductSerializer, self).destroy()
