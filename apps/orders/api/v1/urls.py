from django.urls import path, include
from rest_framework_nested import routers

from . import views

router = routers.SimpleRouter()
router.register(r"orders", views.OrderViewSet, basename="orders")
product_router = routers.NestedSimpleRouter(router, r"orders", lookup="order")
product_router.register(r"products", views.OrderDetailViewSet, basename="products")
urlpatterns = [
    path("", include(router.urls)),
    path("", include(product_router.urls)),
]
