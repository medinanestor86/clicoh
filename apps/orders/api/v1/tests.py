import datetime
from django.urls import reverse
from model_bakery import baker
from rest_framework.test import APITestCase

from apps.orders.models import Order


class OrderAPITestCase(APITestCase):

    def setUp(self) -> None:
        self.user = baker.make("auth.User")
        self.client.force_login(user=self.user)

    def test_list(self):
        baker.make("orders.Order", _quantity=5)
        api_url = reverse("orders-list")
        response = self.client.get(api_url)
        self.assertTrue(response.status_code == 200)
        self.assertTrue(response.json()["count"] == 5)

    def test_retrieve(self):
        order = baker.make("orders.Order")
        api_url = reverse("orders-detail", kwargs={"pk": order.pk})
        response = self.client.get(api_url)
        self.assertTrue(response.status_code == 200)
        self.assertTrue(response.json()["id"] == order.pk, response.json())

    def test_delete(self):
        order = baker.make("orders.Order")
        products = baker.make("products.Product", stock=10, _quantity=5)
        _ = [
            baker.make("orders.OrderDetail", order=order, product=product, quantity=5)
            for product in products
        ]
        total_orders = Order.objects.count()
        api_url = reverse("orders-detail", kwargs={"pk": order.pk})
        response = self.client.delete(api_url)
        self.assertTrue(response.status_code == 204)
        self.assertTrue(Order.objects.count() == total_orders - 1)
        for product in products:
            product.refresh_from_db()
            self.assertEqual(product.stock, 15)

    def test_create(self):
        total_orders = Order.objects.count()
        api_url = reverse("orders-list")
        data = {"date_created": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")}
        response = self.client.post(api_url, data=data)
        self.assertTrue(response.status_code == 201)
        self.assertTrue(Order.objects.count() == total_orders + 1)


class OrderProductAPITestCase(APITestCase):

    def setUp(self) -> None:
        self.user = baker.make("auth.User")
        self.client.force_login(user=self.user)

    def test_list(self):
        order = baker.make("orders.Order")
        baker.make("orders.OrderDetail", order=order, _quantity=5)
        api_url = reverse("products-list", kwargs={"order_pk": order.pk})
        response = self.client.get(api_url)
        self.assertTrue(response.status_code == 200)
        self.assertTrue(response.json()["count"] == 5)

    def test_retrieve(self):
        order = baker.make("orders.Order")
        products = baker.make("orders.OrderDetail", order=order, _quantity=5)
        api_url = reverse("products-detail", kwargs={"order_pk": order.pk, "pk": products[0].pk})
        response = self.client.get(api_url)
        self.assertTrue(response.status_code == 200)
        self.assertTrue(response.json()["id"] == products[0].pk)

    def test_create(self):
        order = baker.make("orders.Order")
        product = baker.make("products.Product", stock=100)
        api_url = reverse("products-list", kwargs={"order_pk": order.pk})
        data = {
            "product": product.pk,
            "quantity": 10
        }
        response = self.client.post(api_url, data=data)
        product.refresh_from_db()
        self.assertTrue(response.status_code == 201, response.__dict__)
        self.assertEqual(product.stock, 90)

    def test_update(self):
        order = baker.make("orders.Order")
        product = baker.make("products.Product", stock=90)
        detail = baker.make("orders.OrderDetail", product=product, order=order, quantity=5)
        api_url = reverse("products-detail", kwargs={"order_pk": order.pk, "pk": detail.pk})
        data = {
            "quantity": 10
        }
        response = self.client.patch(api_url, data=data)
        product.refresh_from_db()
        detail.refresh_from_db()
        self.assertTrue(response.status_code == 200)
        self.assertEqual(detail.quantity, 10)
        self.assertEqual(product.stock, 85)

    def test_delete(self):
        order = baker.make("orders.Order")
        product = baker.make("products.Product", stock=90)
        detail = baker.make("orders.OrderDetail", product=product, order=order, quantity=5)
        api_url = reverse("products-detail", kwargs={"order_pk": order.pk, "pk": detail.pk})
        response = self.client.delete(api_url)
        product.refresh_from_db()
        self.assertTrue(response.status_code == 204)
        self.assertEqual(order.products.count(), 0)
        self.assertEqual(product.stock, 95)
