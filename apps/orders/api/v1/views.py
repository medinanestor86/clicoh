from rest_framework import viewsets, permissions
from django.shortcuts import get_object_or_404
from apps.orders.api.v1.serializers import OrderSerializer, OrderProductSerializer
from apps.orders.models import Order, OrderDetail


class OrderViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = OrderSerializer
    queryset = Order.objects.all()


class OrderDetailViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = OrderProductSerializer
    queryset = OrderDetail.objects.all()

    def get_queryset(self):
        return self.queryset.filter(order_id=self.kwargs["order_pk"])

    def perform_create(self, serializer):
        serializer.save(order=self.get_order())

    def get_order(self):
        return get_object_or_404(Order, pk=self.kwargs["order_pk"])

    def get_serializer_context(self):
        context = super(OrderDetailViewSet, self).get_serializer_context()
        if self.kwargs.get("order_pk"):
            context.update(order=self.get_order())
        return context
