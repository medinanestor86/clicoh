from decimal import Decimal

from django.db import models, transaction

from apps.orders.managers import OrderDetailManager
from services.externals.dolarsi.core import Consumer as DollarSiConsumer
from services.externals.dolarsi.constans import DollarEnum


class Order(models.Model):
    date_created = models.DateTimeField(auto_now=True)

    def get_total(self):
        details = self.products.all()
        return sum([detail.product.price * detail.quantity for detail in details])

    def get_total_usd(self):
        return self.get_total() / Decimal(DollarSiConsumer.get_values()[DollarEnum.DOLLAR_BLUE])

    @transaction.atomic
    def delete(self, using=None, keep_parents=False):
        details = self.products.all()
        for detail in details:
            detail.product.stock += detail.quantity
            detail.product.save()
        super(Order, self).delete(using=using, keep_parents=keep_parents)


class OrderDetail(models.Model):
    order = models.ForeignKey("orders.Order", on_delete=models.CASCADE, related_name="products")
    quantity = models.PositiveIntegerField()
    product = models.ForeignKey("products.Product", on_delete=models.CASCADE)

    objects = OrderDetailManager()

    class Meta:
        indexes = [
            models.Index(fields=["order"]),
        ]

    @transaction.atomic
    def delete(self, using=None, keep_parents=False):
        self.product.stock += self.quantity
        self.product.save()
        super(OrderDetail, self).delete(using=using, keep_parents=keep_parents)
