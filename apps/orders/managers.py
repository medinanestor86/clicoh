from django.db import models, transaction


class OrderDetailManager(models.Manager):

    @transaction.atomic
    def add_product(self, order_id, product_id, quantity):
        obj, created = self.get_or_create(
            order_id=order_id, product_id=product_id, defaults={"quantity": quantity}
        )
        if not created:
            obj.product.stock = obj.product.stock + (obj.quantity - quantity)
            obj.product.save()
            obj.quantity = quantity
            obj.save()
        else:
            obj.product.stock -= quantity
            obj.product.save()
        return obj

