from rest_framework import viewsets, mixins, permissions, status
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.products.api.v1.serializers import ProductModelSerializer, ProductStockSerializer
from apps.products.models import Product


class ProductViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = ProductModelSerializer
    queryset = Product.objects.all()

    @action(detail=True, methods=["patch"])
    def set_stock(self, request, pk=None, *args, **kwargs):
        obj = self.get_object()
        serializer = ProductStockSerializer(obj, data=self.request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status.HTTP_200_OK)
