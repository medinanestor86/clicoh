from rest_framework import serializers

from apps.products.models import Product


class ProductModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = "__all__"


class ProductStockSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ("stock", )
