from rest_framework import routers
from . import views

router = routers.SimpleRouter()
router.register(r'products', views.ProductViewSet, basename="products")
urlpatterns = router.urls
