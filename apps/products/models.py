from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=75)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock = models.PositiveIntegerField()

    class Meta:
        indexes = [
            models.Index(fields=["name"]),
        ]

